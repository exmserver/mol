package com.gitlab.exmserver.mol.nms.v1_19_R1.factory;

import com.gitlab.exmserver.mol.nms.v1_19_R1.entity.MolLivingEntity;

/**
 *
 */
@SuppressWarnings("unused")
public class MolEntitiesFactory extends com.gitlab.exmserver.mol.factory.MolEntitiesFactory {
  @Override
  public com.gitlab.exmserver.mol.entity.MolLivingEntity livingEntity(org.bukkit.entity.LivingEntity livingEntity) {
    return new MolLivingEntity(livingEntity);
  }
}
