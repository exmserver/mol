plugins {
  id("conventions.java")
  id("conventions.maven-publish")
  id("io.papermc.paperweight.userdev") version("1.3.8")
  id("com.github.johnrengelman.shadow") version("7.1.2")
  //id("io.freefair.aggregate-javadoc-jar") version("6.5.1")
}

dependencies {
  compileOnly(project(":core"))
  paperweightDevelopmentBundle("io.papermc.paper:dev-bundle:1.19.2-R0.1-SNAPSHOT")
}

val shadowPublication = publishing.publications.create<MavenPublication>("shadow") {
  from(components["java"])
  //artifact(tasks.aggregateJavadoc)
}

tasks.assemble {
  dependsOn("reobfJar")
}

shadow {
  component(shadowPublication)
}
