import distributions.asConsumer
import distributions.mainSourcesAttributes

plugins {
  id("conventions.java")
  id("conventions.maven-publish")
  id("com.github.johnrengelman.shadow") version("7.1.2")
  id("io.freefair.aggregate-javadoc-jar") version("6.5.1")
}

val mainSources by configurations.creating<Configuration> {
  asConsumer()
  mainSourcesAttributes(objects)
}

dependencies {
  implementation(project(":bukkit_1_19_R1", "reobf")) {
    isTransitive = false
  }
  implementation(project(":core"))

  mainSources(project(":bukkit_1_19_R1"))
  mainSources(project(":core"))
}

tasks {
  shadowJar {
    archiveClassifier.set("")
  }
}

allprojects {
  group = "com.gitlab.exmserver"
  version = "1.0.0-SNAPSHOT"
}
