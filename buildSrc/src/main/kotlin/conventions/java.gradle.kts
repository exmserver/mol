package conventions

import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.kotlin.dsl.`java-library`

plugins {
  `java-library`
}

java {
  toolchain.languageVersion.set(JavaLanguageVersion.of(17))
}
