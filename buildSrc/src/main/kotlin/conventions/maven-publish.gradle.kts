package conventions

import org.gradle.jvm.toolchain.JavaLanguageVersion
import org.gradle.kotlin.dsl.`maven-publish`

plugins {
  `maven-publish`
}

publishing {
  repositories {
    maven(rootProject.layout.buildDirectory.dir("maven-internal")) {
      name = "MavenInternal"
    }
  }
}
