rootProject.name = "buildSrc"

dependencyResolutionManagement {
  repositories {
    mavenCentral()
  }

  pluginManagement {
    repositories {
      gradlePluginPortal()
      mavenCentral()
    }
  }
}
